terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configuring AWS provider
provider "aws" {
    region = "eu-central-1"
}

resource "aws_vpc" "ZergVPC" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "ZergVPC"
  }
}

# - - Internet Gateway - -
resource "aws_internet_gateway" "ZergInternetGateway" {
  vpc_id = aws_vpc.ZergVPC.id

  tags = {
    Name = "ZergInternetGateway"
  }
}

# - - Subnets - -
resource "aws_subnet" "ZergNet1" {
  vpc_id = aws_vpc.ZergVPC.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "eu-central-1a"
  map_public_ip_on_launch = false

  tags = {
    Name = "ZergNet1"
  }
}

resource "aws_subnet" "ZergNet2" {
  vpc_id = aws_vpc.ZergVPC.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "eu-central-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "ZergNet2"
  }
}

resource "aws_subnet" "ZergNet3" {
  vpc_id = aws_vpc.ZergVPC.id
  cidr_block = "10.0.3.0/24"
  availability_zone = "eu-central-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "ZergNet3"
  }
}

# - - Public Route Table - -
resource "aws_route_table" "ZergPublicRouteTable" {
  vpc_id = aws_vpc.ZergVPC.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ZergInternetGateway.id
  }

  tags = {
    Name = "ZergPublicRouteTable"
  }
}

resource "aws_route_table" "ZergPrivateRouteTable" {
  vpc_id = aws_vpc.ZergVPC.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ZergInternetGateway.id
  }

  tags = {
    Name = "ZergPrivateRouteTable"
  }
}

# - - Public Route Table Associations - -
resource "aws_route_table_association" "ZergNet1RouteAssoc1" {
  subnet_id = aws_subnet.ZergNet1.id
  route_table_id = aws_route_table.ZergPrivateRouteTable.id
}

resource "aws_route_table_association" "ZergNet2RouteAssoc2" {
  subnet_id = aws_subnet.ZergNet2.id
  route_table_id = aws_route_table.ZergPublicRouteTable.id
}

resource "aws_route_table_association" "ZergNet3RouteAssoc1" {
  subnet_id = aws_subnet.ZergNet3.id
  route_table_id = aws_route_table.ZergPublicRouteTable.id
}

# - - S3 Bucket - - 
resource "aws_s3_bucket" "ZergLeviathan" {
  bucket = "zerg-leviathan"
  acl    = "private"
  force_destroy = false

  tags = {
    Name        = "ZergLeviathan"
    Environment = "HashiTalks"
  }
}

resource "aws_s3_bucket" "ZergLeviathan2" {
  bucket = "zerg-leviathan2"
  acl    = "private"
  force_destroy = true

  tags = {
    Name        = "ZergLeviathan2"
    Environment = "HashiTalks"
  }
}